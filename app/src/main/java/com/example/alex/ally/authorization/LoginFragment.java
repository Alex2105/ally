package com.example.alex.ally.authorization;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.alex.ally.R;
import com.example.alex.ally.support_classes.Constant;
import com.example.alex.ally.support_classes.MyToast;
import com.example.alex.ally.support_classes.RegularExpressions;
import com.example.alex.ally.vk_registration.VkArrayDates;
import com.example.alex.ally.vk_registration.VkPersonDates;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;

import java.util.Arrays;
import java.util.List;


public class LoginFragment extends Fragment implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, Constant {
    private EditText email, password;
    private ImageButton facebookButton, vkButton, googlePlusButton;
    private Intent intent;
    private Button loginButton;
    private Context context;
    private Profile profile;
    private ProfileTracker profileTracker;
    private LoginManager loginManager;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private ConnectionResult mConnectionResult;
    private int charactersPassword;
    private boolean regularEmail;
    private String[] scope = new String[]{VKScope.EMAIL, VKScope.PHOTOS, VKScope.MESSAGES, VKScope.FRIENDS};
    private String vkPhoto, vkEmail, vkId, vkFirstName, vkSecondName, facebookFirstName, facebookSecondName, facebookId, token, UserPhoto, facebookSocialName, vkSocialName, googleSocialName,
            googleFirstName, googleSecondName, googleId;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        context = getActivity();
        email = (EditText) root.findViewById(R.id.fragment_registration_email);
        password = (EditText) root.findViewById(R.id.fragment_registration_password);
        facebookButton = (ImageButton) root.findViewById(R.id.fragment_login_button_facebook);
        vkButton = (ImageButton) root.findViewById(R.id.fragment_login_button_vk);
        googlePlusButton = (ImageButton) root.findViewById(R.id.fragment_login_button_google_plus);
        loginButton = (Button) root.findViewById(R.id.fragment_login_button_next);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        loginGoogle();
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
        loginButton.setOnClickListener(this);
        vkButton.setOnClickListener(this);
        googlePlusButton.setOnClickListener(this);
        facebookButton.setOnClickListener(this);
        return root;
    }

    private void loginGoogle() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestEmail()
                .requestId()
                .requestProfile()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();
    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }


    @Override
    public void onStop() {
        super.onStop();
        clearEditText();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_login_button_facebook:
                loginFacebook();
                break;
            case R.id.fragment_login_button_google_plus:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case R.id.fragment_login_button_vk:
                VKSdk.login(getActivity(), scope);
                break;

            case R.id.fragment_login_button_next:
                charactersPassword = password.getText().toString().length();
                regularEmail = RegularExpressions.resultRegularExprensionsEmail(email);
                if (charactersPassword < 6 || charactersPassword > 12 || regularEmail == false) {
                    MyToast.error(context, context.getString(R.string.incorrect_email_or_password));
                } else {
                    intent = new Intent(context, com.example.alex.ally.search.Profile.class);
                    startActivity(intent);

//                }

                    break;
                }
        }
    }

    private void clearEditText() {
        password.setText("");
        email.setText("");

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                vkEmail = res.email;
                vkId = res.userId;


                VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, VKApiConst.PHOTO, "first_name,last_name", "photo_id"));
                request.executeSyncWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onError(VKError error) {
                        super.onError(error);
                        Toast.makeText(getActivity(), R.string.do_not_registration_from_vk, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onComplete(VKResponse response) {
                        super.onComplete(response);
                        VKList<VKApiUserFull> list = (VKList<VKApiUserFull>) response.parsedModel;
                        String json = response.responseString;
                        vkjson(json);
                        vkSocialName = "Vk";
                        //socialNetworksRegistration(vkFirstName, vkSecondName, vkSocialName, vkId, UserPhoto);
                    }

                });

            }

            @Override
            public void onError(VKError error) {

                Toast.makeText(getActivity(), R.string.do_not_registration_from_vk, Toast.LENGTH_LONG).show();
            }
        })) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            super.onActivityResult(requestCode, resultCode, data);
        }

    }

    private void vkjson(String json) {
        Gson gson = new Gson();
        VkArrayDates vkArrayDates = gson.fromJson(json, VkArrayDates.class);
        List<VkPersonDates> data = vkArrayDates.getResponse();
        VkPersonDates name = data.get(0);
        vkFirstName = name.getFirst_name();
        vkSecondName = name.getLast_name();
        UserPhoto = name.getPhoto();


    }


    private void loginFacebook() {
        loginManager.logInWithReadPermissions(getActivity(), Arrays.asList(new String[]{"public_profile", "user_friends"}));
        loginManager.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override

                    public void onSuccess(LoginResult loginResult) {
                        if (Profile.getCurrentProfile() == null) {
                            profileTracker = new ProfileTracker() {
                                @Override
                                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                                    facebookFirstName = currentProfile.getFirstName();
                                    facebookSecondName = currentProfile.getLastName();
                                    facebookId = currentProfile.getId();
                                    UserPhoto = String.valueOf(currentProfile.getProfilePictureUri(150, 150));
                                    profileTracker.stopTracking();
                                    facebookSocialName = context.getString(R.string.facebook);
                                    //socialNetworksRegistration(facebookFirstName, facebookSecondName, facebookSocialName, facebookId, UserPhoto);
                                }
                            };
                        } else {
                            profile = Profile.getCurrentProfile();
                            facebookFirstName = profile.getFirstName();
                            UserPhoto = String.valueOf(profile.getProfilePictureUri(150, 150));
                            facebookSecondName = profile.getLastName();
                            facebookId = profile.getId();
                            facebookSocialName = context.getString(R.string.facebook);
                            // socialNetworksRegistration(facebookFirstName, facebookSecondName, facebookSocialName, facebookId, UserPhoto);
                        }


                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(context, R.string.do_not_registration_from_facebook
                                , Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();

                            }
                        }
                    }

                });
    }


    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Person currentUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            String personPhotoUrl = currentUser.getImage().getUrl();
            String email = acct.getEmail();
            googleId = acct.getId();
            String name = acct.getDisplayName();
//           Person mePerson = plusDomains.people().get("me").execute();
            if (name != null) {
                String[] arrayName = name.split(" ");
                googleFirstName = arrayName[0];
                googleSecondName = arrayName[1];
            }
            googleSocialName = "Google";
            //socialNetworksRegistration(googleFirstName, googleSecondName, googleSocialName, googleId, UserPhoto);
        } else {
            Toast.makeText(getActivity(), R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // !!!
                connectionResult.startResolutionForResult(getActivity(), ConnectionResult.DEVELOPER_ERROR);
            } catch (IntentSender.SendIntentException e) {
                mGoogleApiClient.connect();
            }
        }

        mConnectionResult = connectionResult;

    }
}

package com.example.alex.ally.authorization;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alex.ally.R;
import com.example.alex.ally.support_classes.Constant;
import com.example.alex.ally.support_classes.GetCurensyYear;
import com.example.alex.ally.support_classes.MyToast;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Alex on 12.08.2016.
 */
public class UserRegistrationInformation extends Activity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, Constant {
    private CircleImageView foto;
    private GetCurensyYear year;
    private ImageView man, woman;
    private int installedYear, installedMonth, installedDay;
    private String manking, women, selectedImagePath;
    private TextView dayOfBirth;
    private Button registrationButton;
    private Intent intent;
    private Uri mCropImageUri, selectImageUri;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_registration_information);
        foto = (CircleImageView) findViewById(R.id.activity_user_registration_information_user_profile_photo);
        dayOfBirth = (TextView) findViewById(R.id.activity_user_registration_information_day_of_birthday);
        registrationButton = (Button) findViewById(R.id.activity_user_registration_information_button_next);
        man = (ImageView)findViewById(R.id.activity_user_registration_information_photo_man);
        woman = (ImageView)findViewById(R.id.activity_user_registration_information_photo_woman);
        year = new GetCurensyYear();
        context = UserRegistrationInformation.this;
        registrationButton.setOnClickListener(this);
        dayOfBirth.setOnClickListener(this);
        foto.setOnClickListener(this);
        man.setOnClickListener(this);
        woman.setOnClickListener(this);

    }

    private void dataPicker() {
        dayOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int nextYear = year.getCurrentYear() - 80;
                int startYear = year.getCurrentYear() - 18;

                DatePickerDialog birthDay = DatePickerDialog.newInstance(
                        UserRegistrationInformation.this,
                        startYear,
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                birthDay.setAccentColor(Color.parseColor("#03a9f4"));
                birthDay.setThemeDark(true);
                birthDay.dismissOnPause(true);
                birthDay.setYearRange(nextYear, startYear);
                birthDay.show(getFragmentManager(), "Datepickerdialog");
            }

        });
    }

    private void getLink(Intent data) {
        selectImageUri = data.getData();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_user_registration_information_button_next:
                if (selectImageUri == null) {
                    MyToast.info(context, "Select Image");
                } else if (manking == null && women == null) {
                    MyToast.info(context, "Select sex");
                } else if (dayOfBirth.getText().toString().equals(getResources().getString(R.string.day_of_birthday))) {
                    MyToast.info(context, "Select day of birthday");
                } else {
                    intent = new Intent(UserRegistrationInformation.this, Interests.class);
                    startActivity(intent);
                }
                break;
            case R.id.activity_user_registration_information_day_of_birthday:
                dataPicker();
                break;
            case R.id.activity_user_registration_information_user_profile_photo:
                CropImage.startPickImageActivity(this);
                break;
            case R.id.activity_user_registration_information_photo_man:
                man.setImageResource(R.drawable.yellow_man);
                woman.setImageResource(R.drawable.woman);
                manking = "мужской";
                women = null;
                break;
            case R.id.activity_user_registration_information_photo_woman:
                woman.setImageResource(R.drawable.woman_yellow);
                man.setImageResource(R.drawable.man);
                women = "женский";
                manking = null;
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String birthDay = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        installedDay = dayOfMonth;
        installedYear = year;
        installedMonth = monthOfYear;
        dayOfBirth.setText(birthDay);
    }

    public void setDataFromServer(String sex, String day, String mounth, String year) {

    }


    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            getLink(data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                foto.setImageURI(result.getUri());
                Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        dayOfBirth.setText(getResources().getString(R.string.day_of_birthday));
        foto.setImageDrawable(getResources().getDrawable(R.drawable.foto));
        man.setImageDrawable(getResources().getDrawable(R.drawable.man));
        woman.setImageDrawable(getResources().getDrawable(R.drawable.woman));
    }
}


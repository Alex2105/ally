package com.example.alex.ally.vk_registration;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VkArrayDates {

    @SerializedName("response")
    private List<VkPersonDates> response;

    public List<VkPersonDates> getResponse() {
        return response;
    }

    public void setResponse(List<VkPersonDates> response) {
        this.response = response;
    }
}

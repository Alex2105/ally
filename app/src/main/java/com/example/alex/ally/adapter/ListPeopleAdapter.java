package com.example.alex.ally.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alex.ally.R;
import com.example.alex.ally.support_classes.PeopelData;
import com.sackcentury.shinebuttonlib.ShineButton;

import java.util.ArrayList;

/**
 * Created by Alscon on 30.08.2016.
 */
public class ListPeopleAdapter extends BaseAdapter {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<PeopelData> peopleInformation;

    public ListPeopleAdapter(Context context, ArrayList<PeopelData> peopleInformation) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.peopleInformation = peopleInformation;
    }

    @Override
    public int getCount() {
        return peopleInformation.size();
    }

    @Override
    public Object getItem(int position) {
        return peopleInformation.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;
        PeopelData peopelData = getPeopleData(position);
        if (view == null){
            view = layoutInflater.inflate(R.layout.people_item,parent, false);
            viewHolder = new ViewHolder();
            viewHolder.fullName= (TextView)view.findViewById(R.id.people_item_user_name);
            viewHolder.age= (TextView)view.findViewById(R.id.people_item_age);
            viewHolder.distance= (TextView)view.findViewById(R.id.people_item_distance);
            viewHolder.interests= (TextView)view.findViewById(R.id.people_item_interests);
            viewHolder.like = (ShineButton)view.findViewById(R.id.people_item_like);
        }else {
            viewHolder = (ViewHolder)view.getTag();
        }
       viewHolder.fullName.setText(peopelData.fullName);
        viewHolder.age.setText(peopelData.age);
        viewHolder.distance.setText(peopelData.distance);
        viewHolder.interests.setText(peopelData.interests);


        return view;
    }

    PeopelData getPeopleData(int position){
        return ((PeopelData) getItem(position));
    }

    static class ViewHolder{
        TextView fullName;
        TextView age;
        ImageView foto;
        TextView distance;
        TextView interests;
        ShineButton like;

    }
}

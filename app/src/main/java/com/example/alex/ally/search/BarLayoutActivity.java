package com.example.alex.ally.search;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import com.example.alex.ally.R;
import com.example.alex.ally.adapter.ViewPageAdapter;
import com.example.alex.ally.search.tabs_fragment.Chat;
import com.example.alex.ally.search.tabs_fragment.People;

/**
 * Created by Alscon on 24.08.2016.
 */
public class BarLayoutActivity extends AppCompatActivity {
    private ViewPager viewpage;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bar_layout);
        viewpage = (ViewPager) findViewById(R.id.activity_bar_layout_view_page);
        tabLayout = (TabLayout) findViewById(R.id.activity_bar_layout_table);
        setupViewPage(viewpage);
        tabLayout.setupWithViewPager(viewpage);

    }

    private void setupViewPage(ViewPager viewpage) {
        ViewPageAdapter adapter = new ViewPageAdapter(getSupportFragmentManager());
        adapter.addFragment(People.newInstance("this is first fragment"), "One");
        adapter.addFragment(Chat.newInstance("this is second fragment"), "Two");
       // adapter.addFragment(Map.newInstance("this is third fragment"), "Three");
        viewpage.setAdapter(adapter);

    }
}

package com.example.alex.ally.search.tabs_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.alex.ally.R;
import com.example.alex.ally.adapter.ListPeopleAdapter;
import com.example.alex.ally.support_classes.Constant;
import com.example.alex.ally.support_classes.PeopelData;

import java.util.ArrayList;

/**
 * Created by Alex on 15.08.2016.
 */
public class People extends Fragment implements Constant {

    private ListView listView;
    private String peopleExampleDate;
    private ListPeopleAdapter listPeopleAdapter;
    ArrayList<PeopelData> list;
    ArrayList<PeopelData> fff;

    public People() {
    }

    public static People newInstance(String exapleArgument) {
        People people = new People();
        Bundle argm = new Bundle();
        argm.putString(ARG_EXAMPLE, exapleArgument);
        people.setArguments(argm);
        return people;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        peopleExampleDate = getArguments().getString(ARG_EXAMPLE);
        Log.i("Fragment create with ", peopleExampleDate);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_people, container, false);
        listView = (ListView)root.findViewById(R.id.activity_people_list);
        list = new ArrayList<>();
        fff = new ArrayList<>();
        listPeopleAdapter = new ListPeopleAdapter(getActivity(),list);
        listView.setAdapter(listPeopleAdapter);
        return root;
    }


}

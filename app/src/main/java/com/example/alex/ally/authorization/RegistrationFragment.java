package com.example.alex.ally.authorization;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.alex.ally.R;
import com.example.alex.ally.support_classes.MyToast;
import com.example.alex.ally.support_classes.RegularExpressions;


public class RegistrationFragment extends Fragment implements View.OnClickListener {
    private EditText fullName, email, password;
    private Button next;
    private Intent intent;
    private Context context;
    private int charactersPassword;
    private int charactersUserName;
    private boolean regularEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_registration, container, false);
        context = getActivity();
        fullName = (EditText) root.findViewById(R.id.fragment_registration_full_name);
        email = (EditText) root.findViewById(R.id.fragment_registration_email);
        password = (EditText) root.findViewById(R.id.fragment_registration_password);
        next = (Button) root.findViewById(R.id.fragment_registration_button_next);
        email.clearComposingText();
        next.setOnClickListener(this);
        return root;
    }

    @Override
    public void onStop() {
        super.onStop();
        clearEdidText();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_registration_button_next:
                charactersPassword = password.getText().toString().length();
                charactersUserName = fullName.getText().toString().length();
                regularEmail = RegularExpressions.resultRegularExprensionsEmail(email);
                if (charactersUserName < 2 || charactersUserName>50 ) {
                    MyToast.info(context, context.getString(R.string.size_user_name));
                }else if (regularEmail == false){
                    MyToast.info(context, context.getString(R.string.incorrect_email));
                } else if (charactersPassword < 6 || charactersPassword > 21) {
                    MyToast.info(context, context.getString(R.string.size_password));

                }else {
                    intent = new Intent(context, UserRegistrationInformation.class);
                    startActivity(intent);
                }
                break;
        }
    }

    private void clearEdidText() {
        fullName.setText("");
        email.setText("");
        password.setText("");
    }
}

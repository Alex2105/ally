package com.example.alex.ally.authorization;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.example.alex.ally.R;
import com.example.alex.ally.support_classes.MyToast;
import com.toptoche.multiselectwidget.MultiSelectFragment;
import com.toptoche.multiselectwidget.MultiSelectView;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import java.util.ArrayList;


public class Interests extends Activity implements View.OnClickListener {

    private String userPick, beterSpinerCheck;
    private EditText other;
    private MultiSelectView interests;
    private BetterSpinner country, searchRadius;
    private AutoCompleteTextView city;
    private Button apply;
    private String[] userInteres = {"Рыбалка", "Прогулки", "Покемоны"};
    private int spinner_images[] = {R.drawable.email, R.drawable.email, R.drawable.email};
    private ArrayAdapter<String> adapter, cityAdapter, radiusAdapter;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interests);
        country = (BetterSpinner) findViewById(R.id.activity_interests_country);
        city = (AutoCompleteTextView) findViewById(R.id.activity_interests_city);
        searchRadius = (BetterSpinner) findViewById(R.id.activity_interests_search_radius);
        interests = (MultiSelectView) findViewById(R.id.activity_interests_interests);
        other = (EditText) findViewById(R.id.activity_profile_message);
        apply = (Button) findViewById(R.id.activity_interests_button_apply);
        setcountryAdapter();
        setCityAdapter();
        setradiusAdapter();
        apply.setOnClickListener(this);
        mContext = Interests.this;
        setMultiSelectView();
    }

    @Override
    protected void onStop() {
        super.onStop();
        country.setText("");
        city.setText("");
        searchRadius.setText("");
        interests.setText("");
        other.setText("");
    }

    private void setCityAdapter() {
        String cityList[] = getResources().getStringArray(R.array.city);
        cityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, cityList);
        city.setAdapter(cityAdapter);
    }

    private void setcountryAdapter() {
        String countryList[] = getResources().getStringArray(R.array.country);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, countryList);
        country.setAdapter(adapter);
    }

    private void setradiusAdapter() {
        String radiusList[] = getResources().getStringArray(R.array.radius);
        radiusAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, radiusList);
        searchRadius.setAdapter(radiusAdapter);

    }

    private void setMultiSelectView() {
        interests.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1,
                getResources().getStringArray(R.array.interests)), new ArrayList());
        interests.setDelimiter("   ");
        interests.setTitle("Select Item");

        interests.setPositiveButton("Ok");
        interests.setPositiveButton("Ok", new MultiSelectFragment.OnPositiveButtonClicked() {
            @Override
            public void onPositiveButtonClicked(Dialog dialog) {
                dialog.dismiss();

            }
        });
        interests.setNegativeButton("Cancel");
        interests.setNegativeButton("CANCEL", new MultiSelectFragment.OnNegativeButtonClicked() {
            @Override
            public void onNegativeButtonClicked(Dialog dialog) {
                dialog.dismiss();
            }
        });

        interests.setOnNoItemSelectedListener(new MultiSelectFragment.OnNoItemSelected() {
            @Override
            public void onNoItemSelected(Dialog dialog) {

            }
        });
    }

//    private ArrayList allCountry(){
//        Locale[] locales = Locale.getAvailableLocales();
//        ArrayList<String> countries = new ArrayList<String>();
//        for (Locale locale : locales) {
//            String country = locale.getDisplayCountry();
//            if (country.trim().length()>0 && !countries.contains(country)) {
//                countries.add(country);
//            }
//        }
//        Collections.sort(countries);
//
//        return  countries;
//    }
//ValueAnimator valueAnimator = new ValueAnimator();
//    valueAnimator.setInterpolator(new EasingInterpolator(Ease.BOUNCE_OUT));
//
//    ObjectAnimator animator = ObjectAnimator.ofFloat(city, "transitionY", 0, 100);
//    animator.setInterpolator(new EasingInterpolator(Ease.BOUNCE_OUT));
//    animator.start();
//    valueAnimator.start();


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_interests_button_apply:

                if (country.getText().toString().length() == 0) {
                    MyToast.info(mContext, getString(R.string.select_country));
                } else if (city.getText().toString().length() == 0) {
                    MyToast.info(mContext, getString(R.string.select_city));
                } else if (searchRadius.getText().toString().length() == 0) {
                    MyToast.info(mContext, "Select distance");
                } else if (interests.getText().toString().length() == 0) {
                    MyToast.info(mContext, getString(R.string.select_interests));
                }
                //  prepareObjectAnimator(bounceInterpolator);
                break;


        }

    }
}






package com.example.alex.ally.search.tabs_fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.alex.ally.R;
import com.example.alex.ally.support_classes.Constant;

/**
 * Created by Alex on 15.08.2016.
 */
public class Chat extends Fragment implements Constant {
    private String chatExampleDate;

    public Chat() {
    }

    public static Chat newInstance(String exapleArgument) {
        Chat chat = new Chat();
        Bundle argm = new Bundle();
        argm.putString(ARG_EXAMPLE, exapleArgument);
        chat.setArguments(argm);
        return chat;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chatExampleDate = getArguments().getString(ARG_EXAMPLE);
        Log.i("Fragment create with ", chatExampleDate);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_chat, container, false);
        return root;
    }


}

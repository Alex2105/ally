package com.example.alex.ally.support_classes;

/**
 * Created by Alscon on 30.08.2016.
 */
public class PeopelData {
    public String fullName;
    public String age;
    public String imageFoto;
    public String interests;
    public String distance;

    public PeopelData(String fullName, String age, String imageFoto, String interests, String distance) {
        this.fullName = fullName;
        this.age = age;
        this.imageFoto = imageFoto;
        this.interests = interests;
        this.distance = distance;
    }
}
